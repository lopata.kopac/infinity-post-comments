import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { postListReducer } from './core/store/post-list/post-list.reducers';
import { PostListEffects } from './core/store/post-list/post-list.effects';
import { HttpClientModule } from '@angular/common/http';
import { postListKey } from './core/store/post-list/post-list.selectors';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({ [postListKey]: postListReducer }, {}),
    EffectsModule.forRoot([PostListEffects]),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
