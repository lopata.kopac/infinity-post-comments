import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { postListLoadNext } from './core/store/post-list/post-list.accions';
import { selectPostListData, selectPostListLoadingStatus } from './core/store/post-list/post-list.selectors';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  posts$: Observable<any>;
  loadingStatus$: Observable<string>;
 
  constructor(private store: Store<any>) {
    this.posts$ = store.select(selectPostListData);
    this.loadingStatus$ = store.select(selectPostListLoadingStatus);
  }

  ngOnInit() {
    this.store.dispatch(postListLoadNext());
  }

  loadNextHandle(){
    this.store.dispatch(postListLoadNext());
  }

}
