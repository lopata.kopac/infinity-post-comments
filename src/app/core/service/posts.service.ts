import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostsService{
  apiUrp = 'https://gorest.co.in//public/v2';

  constructor(private http: HttpClient) { }

  getPosts(page = 1, perPage = 20){
    return this.http.get(`${this.apiUrp}/posts?page=${page}&per_page=${perPage}`);
  }

  getPost(id: string | number){
    return this.http.get(`${this.apiUrp}/posts/${id}`);
  }
}
