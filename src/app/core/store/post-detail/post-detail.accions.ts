import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const PostDetailActions = createActionGroup({
    source: 'Posts List API',
    events: {
      'Load': emptyProps(),
      'Load Success': props<{post: any}>(),
      'Load Failed': props<{error: any | null}>(),
      'Reset': emptyProps(),
    }
});

export const {
  load : postDetailLoad,
  loadSuccess : postDetailLoadSuccess,
  loadFailed : postDetailLoadFailed,
  reset : postDetailReset,
} = PostDetailActions;