import { createReducer, on } from '@ngrx/store';
import { PostDetailActions } from './post-detail.accions';

export interface PostDetailState {
    data: any | null;
    status: 'loading' | 'loaded' | 'failed' | 'initial';
    error: any | null;
}

export const initialState: PostDetailState = {
    data: null,
    status: 'initial',
    error: null,
};

export const postDetailReducer = createReducer(
    initialState,
    on(PostDetailActions.load, (state): PostDetailState => ({
        ...state,
        status: 'loading',
        error: null
    })),
    on(PostDetailActions.loadSuccess, (state, action): PostDetailState => ({
        ...state,
        status: 'loaded',
        data: action.post
    })),
    on(PostDetailActions.loadFailed, (state, action): PostDetailState => ({...state, status: 'failed', error: action.error})),
);