import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY, catchError, exhaustMap, map } from 'rxjs';
import { PostsService } from '../../service/posts.service';
import { PostDetailActions } from './post-detail.accions';
import { Store } from '@ngrx/store';

@Injectable()
export class PostDetailEffects {
  constructor(
    private actions$: Actions,
    private postsService: PostsService,
    private store: Store<any>
  ) {}
 
  loadPost$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostDetailActions.load),
      exhaustMap(() => {
        return this.postsService.getPost('').pipe(
          map(post => PostDetailActions.loadSuccess({ post })),
          catchError(() => EMPTY)
        );
      })
    );
  });
}