import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PostDetailState } from './post-detail.reducers';

export const postDetailKey = 'postDetail';

export const selectPostDetail = createFeatureSelector<PostDetailState>(postDetailKey);

export const selectPostDetailData = createSelector(
    selectPostDetail,
    (state) => state.data
);

export const selectPostDetailLoadingStatus = createSelector(
    selectPostDetail,
    (state) => state.status
);

export const selectPostDetailIsLoading= createSelector(
    selectPostDetail,
    (state) => state.status === 'loading'
);