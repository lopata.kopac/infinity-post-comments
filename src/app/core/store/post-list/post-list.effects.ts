import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, concatLatestFrom } from '@ngrx/effects';
import { EMPTY, catchError, exhaustMap, map } from 'rxjs';
import { PostsService } from '../../service/posts.service';
import { PostListActions } from './post-list.accions';
import { Store } from '@ngrx/store';
import { selectPostListPagination } from './post-list.selectors';

@Injectable()
export class PostListEffects {
  constructor(
    private actions$: Actions,
    private postsService: PostsService,
    private store: Store<any>
  ) {}
 
  loadPosts$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(PostListActions.loadNext),
      concatLatestFrom(() => this.store.select(selectPostListPagination)),
      exhaustMap(([, {page, perPage}]) => {
        return this.postsService.getPosts(page, perPage).pipe(
          map(posts => PostListActions.loadSuccess({ posts })),
          catchError(() => EMPTY)
        );
      })
    );
  });
}