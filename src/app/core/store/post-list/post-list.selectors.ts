import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PostListState } from './post-list.reducers';

export const postListKey = 'postList';

export const selectPostList = createFeatureSelector<PostListState>(postListKey);

export const selectPostListData = createSelector(
    selectPostList,
    (state) => state.data
);

export const selectPostListLoadingStatus = createSelector(
    selectPostList,
    (state) => state.status
);

export const selectPostListIsLoading= createSelector(
    selectPostList,
    (state) => state.status === 'loading'
);

export const selectPostListPagination= createSelector(
    selectPostList,
    (state) => ({
        page: state.page,
        perPage: state.perPage,
    })
);