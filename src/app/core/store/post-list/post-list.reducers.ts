import { createReducer, on } from '@ngrx/store';
import { PostListActions } from './post-list.accions';

export interface PostListState {
    page: number;
    perPage: number;
    filter: any;
    data: any[];
    status: 'loading' | 'loaded' | 'failed' | 'initial';
    error: any | null;
}

export const initialState: PostListState = {
    page: 0,
    perPage: 10,
    filter: {},
    data: [],
    status: 'initial',
    error: null,
};

export const postListReducer = createReducer(
    initialState,
    on(PostListActions.loadNext, (state): PostListState => ({
        ...state,
        status: 'loading',
        page: state.page + 1,
        error: null
    })),
    on(PostListActions.loadSuccess, (state, action): PostListState => ({
        ...state,
        status: 'loaded',
        data: [...state.data, ...action.posts]
    })),
    on(PostListActions.loadFailed, (state, action): PostListState => ({...state, status: 'failed', error: action.error})),
    on(PostListActions.changeFilter, (state, action): PostListState => ({...state, filter: action.filter})),
);