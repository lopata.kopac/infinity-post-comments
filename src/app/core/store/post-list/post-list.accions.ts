import { createActionGroup, emptyProps, props } from '@ngrx/store';

export const PostListActions = createActionGroup({
    source: 'Posts List API',
    events: {
      'Load Next': emptyProps(),
      'Load Success': props<{posts: any}>(),
      'Load Failed': props<{error: any | null}>(),
      'Change Filter': props<{filter: any}>(),
      'Reset': emptyProps(),
    }
});

export const {
  loadNext : postListLoadNext,
  loadSuccess : postListLoadSuccess,
  loadFailed : postListLoadFailed,
  changeFilter : PostListChangeFitler,
  reset : postListReset,
} = PostListActions;